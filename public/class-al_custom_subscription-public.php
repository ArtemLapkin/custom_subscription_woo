<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/public
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Custom_Subscription_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private static $category_slug;
	private static $subscr_active_meta_field;
	private static $subscr_waits_activation_meta_field;
	private static $subscr_end_date_meta_field;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		self::$category_slug = 'al_subscription';
		self::$subscr_active_meta_field = 'al_subscr_active';
		self::$subscr_waits_activation_meta_field = 'al_subscr_waits_activation';
		self::$subscr_end_date_meta_field = 'al_subscr_end_date';

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Custom_Subscription_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Custom_Subscription_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/Al_Custom_Subscription-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Custom_Subscription_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Custom_Subscription_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/Al_Custom_Subscription-public.js', array( 'jquery' ), $this->version, false );

	}

    // todo doesn't work yet
    public function disable_buy_button() {
        if (is_singular('product') && Al_Custom_Subscription_Actions::does_subscr_wait_activation(get_current_user_id())) {
            add_filter('woocommerce_is_purchasable', '__return_false', 1, 2);
        }
    }



    public static function add_to_cart_validation($passed, $product_id) {
        $product_add = wc_get_product( $product_id );
        if( $product_add->is_type('al_subscription') ) {

            if( Al_Custom_Subscription_Actions::is_subscription_active(get_current_user_id()) ) {
                wc_add_notice(__('You have running subscription', 'al_custom_subscription'), 'error');
                $passed = false;
            } elseif( Al_Custom_Subscription_Actions::does_subscr_wait_activation(get_current_user_id()) ) {
                wc_add_notice(__('Your previous subscription order isn\'t completed yet, please wait or contact us', 'al_custom_subscription'), 'error');
                $passed = false;
            }

            foreach (WC()->cart->get_cart() as $cart_item_key=>$cart_item ){
                $_product = wc_get_product( $cart_item['product_id'] );
                if( $_product->is_type('al_subscription') ) {
                    // todo add cart link
                    wc_add_notice(__('Sorry, you already have subscription in the cart. Please complete the order.', 'al_custom_subscription'), 'error');
                    $passed = false;
                }
            }
        }

        return $passed;
    }

    public static function account_page_tabs($tabs) {
        if( isset( $tabs['customer-logout'] )) {
            unset( $tabs['customer-logout'] );
            $tabs['al_subscription'] = __('Subscription', 'al_custom_subscription');
            $tabs['customer-logout'] = __('Logout', 'al_custom_subscription');
        }else {
            $tabs['al_subscription'] = __('Subscription', 'al_custom_subscription');
        }

        return $tabs;        
    }


    public static function account_page_tab_endpoint() {
        add_rewrite_endpoint( 'al_subscription', EP_ROOT | EP_PAGES );
        flush_rewrite_rules();
    }

    public function account_subscription_page() {
        $user_id = get_current_user_id();
        if( Al_Custom_Subscription_Actions::does_subscr_wait_activation( $user_id ) ) {
            echo '<p>' . __('Your subscription waits activation, order is not completed yet', 'al_custom_subscription') . '</p>' ; echo '<br>';
        }elseif( Al_Custom_Subscription_Actions::is_subscription_active( $user_id ) ) {
            $subsr_end_date = get_user_meta($user_id, Al_Custom_Subscription_Actions::$subscr_end_date_meta_field, true);
            echo '<p>' . __('Your subscription is active. Expire date - ', 'al_custom_subscription') . date('Y-m-d', $subsr_end_date) . '</p>' ;
        } else {
            echo '<p>' . __('Subscription is not active. You can select plan here ', 'al_custom_subscription'). '<a href="'.get_permalink( 48027 ).'">' . __('Membership', 'al_custom_subscription'). '</a></p>' ;

        }
    }

    public static function query_vars($vars) {
        $vars[] = 'al_subscription';
        return $vars;
    }


}
