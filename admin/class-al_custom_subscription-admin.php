<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/admin
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Custom_Subscription_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Custom_Subscription_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Custom_Subscription_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/Al_Custom_Subscription-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Custom_Subscription_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Custom_Subscription_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/Al_Custom_Subscription-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Add product type
     */
    public function add_product_type() {
        al_subscription_class_extend();
	}

    public function show_product_type( $type ) {
        $type[ 'al_subscription' ] = __( 'Subscription', 'al_custom_subscription' );
        return $type;
	}

    public static function load_product_type_class($classname, $product_type) {
        if ( $product_type == 'al_subscription' ) {
            $classname = 'WC_Product_Al_Subscription';
        }
        return $classname;
    }

    /**
     * JS to show product price for new added custom product type
     */
    public static function product_type_js() {
        if ( 'product' != get_post_type() ) :
            return;
        endif;

        ?><script type='text/javascript'>
            jQuery( document ).ready( function() {
                jQuery( '.options_group.pricing' ).addClass( 'show_if_al_subscription' ).show();
            });
        </script><?php
    }

    /**
     * needed for custom product type, to add it to cart
     */
    public static function add_to_cart_custom_product() {
        wc_get_template( 'single-product/add-to-cart/simple.php' );
    }

    /**
     * Subscription tab on edit product page
     * @param $tabs
     * @return mixed
     */
    public static function edit_custom_product_tabs($tabs) {
        $tabs['al_subscription'] = array(
            'label'		=> __( 'Subscription', 'al_custom_subscription' ),
            'target'	=> 'al_subscription_options',
            'class'		=> array( 'show_if_simple_al_subscription' ),
        );

        return $tabs;
    }

    /**
     * Subscription duration select box on edit product page
     */
    public static function edit_custom_product_tab_fields() {
        ?>
        <div id='al_subscription_options' class='panel woocommerce_options_panel'>
            <div class='options_group'><?php
                woocommerce_wp_select( array(
                    'id'			=> '_al_subscription_period',
                    'label'			=> __( 'Subscription Period', 'al_custom_subscription' ),
                    'name'		    => '_al_subscription_period',
                    'desc_tip'	    => __( 'A handy description field', 'al_custom_subscription' ),
                    'options' 	    => array(
                        '1' => '1 month',
                        '6' => '6 month',
                        '12' => '12 month',
                    ),
                ) );
                ?>
            </div>
        </div><?php
    }

    /**
     * Save subscription period field
     * @param $post_id
     */
    public static function save_custom_product_tab_fields($post_id) {
        if ( isset( $_POST['_al_subscription_period'] ) ) {
            update_post_meta( $post_id, '_al_subscription_period', sanitize_text_field( $_POST['_al_subscription_period'] ) );
        }
    }

    /**
     * Order is completed - activate subscription
     * @param $order_id
     */
	public static function order_completed( $order_id ){
        self::order_was_successful($order_id);
    }
    public static function order_processing($order_id) {
        self::order_was_successful($order_id);
    }

    // failed/cancelled/refunded orders
    public static function order_failed($order_id) {
        self::order_wasnt_successfull($order_id);
    }
    public static function order_canceled($order_id) {
        self::order_wasnt_successfull($order_id);
    }
    public static function order_refunded($order_id) {
        self::order_wasnt_successfull($order_id);
    }

    /**
     * Order wasn't completed - remove 'Waits activation' status
     * @param $order_id
     */
    public static function order_wasnt_successfull($order_id) {
        $subscription_prod_id = Al_Custom_Subscription_Actions::does_order_have_subscription_product( $order_id );

        if ( $subscription_prod_id !== false ) {
            $user_id = get_post_meta($order_id, '_customer_user', true);
            if( Al_Custom_Subscription_Actions::does_subscr_wait_activation($user_id) && ! Al_Custom_Subscription_Actions::is_subscription_active($user_id) ) {
                // todo maybe add user role check
                Al_Custom_Subscription_Actions::remove_subscr_waits_activation($user_id);
            }
        }
    }

    public static function order_was_successful($order_id) {
        $subscription_prod_id = Al_Custom_Subscription_Actions::does_order_have_subscription_product( $order_id );
        if ( $subscription_prod_id !== false ) {
            $user_id = get_post_meta($order_id, '_customer_user', true);
            if( Al_Custom_Subscription_Actions::does_subscr_wait_activation($user_id) && ! Al_Custom_Subscription_Actions::is_subscription_active($user_id) ) {
                // todo maybe add user role check
                Al_Custom_Subscription_Actions::remove_subscr_waits_activation($user_id);
                Al_Custom_Subscription_Actions::enable_subscription($user_id, $subscription_prod_id);
            }
        }
    }

    /**
     * Fields added to wp-admin/profile.php
     * @param $user
     */
    public static function user_profile_subscription_fields( $user ) {
        $subscr_active = Al_Custom_Subscription_Actions::is_subscription_active($user->id);

        $end_date = get_user_meta($user->id, Al_Custom_Subscription_Actions::$subscr_end_date_meta_field, true);
        ?>
        <h3><?php _e('User Subscription', 'al_custom_subscription'); ?></h3>

        <table class="form-table">
            <tr>
                <th><label for="address"><?php _e('Subscription is active', 'al_custom_subscription'); ?></label></th>
                <td>
                    <label><input type="radio" name="al_subscription_active" <?php checked($subscr_active , true, true); ?> value="1" /><?php _e('Yes', 'al_custom_subscription'); ?></label><br />
                    <label><input type="radio" name="al_subscription_active" <?php checked( $subscr_active, false, true); ?> value="0" /><?php _e('No', 'al_custom_subscription'); ?></label><br />
                </td>
            </tr>
            <tr>
                <th><label for="address"><?php _e('Subscription end date', 'al_custom_subscription'); ?></label></th>
                <td>
                    <?php if( $end_date ){ ?>
                        <p><?php echo date('Y-m-d H:i:s', $end_date) ?></p>
                    <?php }else { ?>
                        <p><?php _e('No end date', 'al_custom_subscription'); ?></p>
                    <?php } ?>
                </td>
            </tr>
        </table>
        <?php
    }

    /**
     * Save user on wp-admin/profile.php
     * @param $user_id
     * @return bool
     */
    public static function save_user_profile($user_id) {
        if ( !current_user_can( 'edit_user', $user_id ) ) {
            return false;
        }
        update_user_meta( $user_id, Al_Custom_Subscription_Actions::$subscr_active_meta_field, intval($_POST['al_subscription_active']) );
    }


    /**
     *
     * @param $order_id
     */
    public static function order_created($order_id) {

        if( Al_Custom_Subscription_Actions::does_order_have_subscription_product($order_id) ) {
            $order = wc_get_order( $order_id );
            $user_id = $order->get_user_id();
            Al_Custom_Subscription_Actions::set_subscr_waits_activation($user_id);
        }
    }
}


function al_subscription_class_extend(){
    class WC_Product_Al_Subscription extends WC_Product {
        public function __construct($product) {
            $this->product_type = 'al_subscription';
            parent::__construct($product);
        }

        /**
         * Get internal type.
         *
         * @return string
         */
        public function get_type() {
            return 'al_subscription';
        }
    }
}
