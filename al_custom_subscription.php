<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Al_Custom_Subscription
 *
 * @wordpress-plugin
 * Plugin Name:       AL Custom Subscritpion
 * Plugin URI:        #
 * Description:       Custom subscription system
 * Version:           1.0.0
 * Author:            Artem Lapkin
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Al_Custom_Subscription
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-al_custom_subscription-activator.php
 */
function activate_al_custom_subscription() {
    if( al_check_if_woo_active() ) {
        deactivate_al_custom_subscription();
        // Throw an error in the wordpress admin console
        $error_message = __('This plugin requires <a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> &amp; plugin to be active!', 'al_custom_subscription');
        die($error_message);
    }
//    al_custom_subscription_rewrite_rules();
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-al_custom_subscription-activator.php';
	Al_Custom_Subscription_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-al_custom_subscription-deactivator.php
 */
function deactivate_al_custom_subscription() {
//    al_custom_subscription_rewrite_rules();
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-al_custom_subscription-deactivator.php';
	Al_Custom_Subscription_Deactivator::deactivate();
}
function al_custom_subscription_rewrite_rules() {
    add_rewrite_endpoint( 'al_subscription', EP_ROOT | EP_PAGES );
    flush_rewrite_rules();
}
function al_check_if_woo_active(){
    return !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
}
register_activation_hook( __FILE__, 'activate_al_custom_subscription' );
register_deactivation_hook( __FILE__, 'deactivate_al_custom_subscription' );

/**
 * The code that works with subscriptions, enable,disable,check
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-al_custom_subscription_actions.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-al_custom_subscription.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_al_custom_subscription() {

	$plugin = new Al_Custom_Subscription();
	$plugin->run();

}
run_al_custom_subscription();
