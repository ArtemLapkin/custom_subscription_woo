<?php

/**
 * Fired during plugin deactivation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Custom_Subscription_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
