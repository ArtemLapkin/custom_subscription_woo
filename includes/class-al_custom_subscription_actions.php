<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Custom_Subscription_Actions {

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    public static $category_slug = 'al_subscription';
    public static $product_type = 'al_subscription';
    public static $subscr_active_meta_field = 'al_subscr_active';
    public static $subscr_waits_activation_meta_field = 'al_subscr_waits_activation';
    public static $subscr_end_date_meta_field = 'al_subscr_end_date';

    public static function does_order_have_subscription_product($order_id) {
        $order = wc_get_order( $order_id );

        foreach ($order->get_items() as $item) {
            $terms = wp_get_object_terms(($item['product_id']), 'product_type');
            if( $terms && sanitize_title( current( $terms )->name ) ==  'al_subscription' ) {
                return $item['product_id'];
            }
        }
        return false;
    }

    /**
     * Gives subscription category object
     * @return mixed
     */
    public static function get_category_object() {
        return get_term_by('slug', self::$category_slug, 'product_cat');
    }

    /**
     * Disables subscription
     * @param $user_id
     */
    public static function disable_subscription( $user_id ) {
        update_user_meta($user_id, self::$subscr_active_meta_field, 0);
        update_user_meta($user_id, self::$subscr_end_date_meta_field, 0);
    }

    /**
     * Enables subscription
     * @param $user_id
     */
    public static function enable_subscription( $user_id, $prod_id ) {
        $subscription_period = get_post_meta($prod_id, '_al_subscription_period', true);
        if( $subscription_period == false ) {
            return;
        }
        update_user_meta($user_id, self::$subscr_active_meta_field, 1);
        update_user_meta($user_id, self::$subscr_end_date_meta_field, strtotime("+ $subscription_period months") );
    }

    /**
     * Checks if subscription is active
     * @param $user_id
     * @return bool
     */
    public static function is_subscription_active($user_id) {
        $subsr_active = get_user_meta($user_id, self::$subscr_active_meta_field, true);
        $subsr_end_date = get_user_meta($user_id, self::$subscr_end_date_meta_field, true);

        $out = $subsr_active == 1 && $subsr_end_date > time();
        return $out;
    }

    /**
     * Removes attribute - subscription waits activation
     * @param $user_id
     */
    public static function remove_subscr_waits_activation($user_id) {
        update_user_meta($user_id, self::$subscr_waits_activation_meta_field, 0);
    }

    /**
     * Sets attribute - subscription waits activation
     * @param $user_id
     */
    public static function set_subscr_waits_activation($user_id) {

        update_user_meta($user_id, self::$subscr_waits_activation_meta_field, 1);
    }

    /**
     * Checks if subscription is going to be activated. When order with subscription product already exists
     * @param $user_id
     * @return bool
     */
    public static function does_subscr_wait_activation($user_id) {
        return get_user_meta($user_id, self::$subscr_waits_activation_meta_field, true) == 1;
    }


    /**
     * Checks if customer has not completed orders with subscription product
     * @param $user_id
     * @return bool
     */
    public static function does_user_have_not_completed_order_subscription( $user_id ) {

        $statuses = self::get_not_completed_order_statuses();

        $customer_orders = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => wc_get_order_types(),
            'post_status' => $statuses
        ) );

        if( empty($customer_orders) ) {
            return false;
        }

        foreach ($customer_orders as $customer_order) {
            if( self::does_order_have_subscription_product($customer_order->ID) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns orders not completed statuses
     * @return array
     */
    private static function get_not_completed_order_statuses() {
        return array('wc-pending', 'wc-processing', 'wc-on-hold');

    }

}
