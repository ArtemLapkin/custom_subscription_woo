<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Al_Custom_Subscription
 * @subpackage Al_Custom_Subscription/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Custom_Subscription {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Al_Custom_Subscription_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'Al_Custom_Subscription';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Al_Custom_Subscription_Loader. Orchestrates the hooks of the plugin.
	 * - Al_Custom_Subscription_i18n. Defines internationalization functionality.
	 * - Al_Custom_Subscription_Admin. Defines all hooks for the admin area.
	 * - Al_Custom_Subscription_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-al_custom_subscription-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-al_custom_subscription-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-al_custom_subscription-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-al_custom_subscription-public.php';

		$this->loader = new Al_Custom_Subscription_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Al_Custom_Subscription_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Al_Custom_Subscription_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Al_Custom_Subscription_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		// new product type
		$this->loader->add_action( 'init', $plugin_admin, 'add_product_type',1 );
		$this->loader->add_filter( 'product_type_selector', $plugin_admin, 'show_product_type',1 );
		$this->loader->add_filter( 'woocommerce_product_class', $plugin_admin, 'load_product_type_class', 1, 2 );
		$this->loader->add_action( 'admin_footer', $plugin_admin, 'product_type_js');
		$this->loader->add_action('woocommerce_al_subscription_add_to_cart', $plugin_admin, 'add_to_cart_custom_product');
		$this->loader->add_filter('woocommerce_product_data_tabs', $plugin_admin, 'edit_custom_product_tabs');
		$this->loader->add_filter('woocommerce_product_data_panels', $plugin_admin, 'edit_custom_product_tab_fields');
		$this->loader->add_action('woocommerce_process_product_meta_al_subscription', $plugin_admin, 'save_custom_product_tab_fields');

		// order created
        $this->loader->add_action( 'woocommerce_checkout_update_order_meta', $plugin_admin, 'order_created', 100 );
        // update subscription on update order status action
        $this->loader->add_action('woocommerce_order_status_completed', $plugin_admin, 'order_completed' );
        $this->loader->add_action('woocommerce_order_status_processing', $plugin_admin, 'order_processing' );
        // remove 'Waits activation' status if order was cancelled, refunded, failed
        $this->loader->add_action('woocommerce_order_status_cancelled', $plugin_admin, 'order_canceled' );
        $this->loader->add_action('woocommerce_order_status_failed', $plugin_admin, 'order_failed' );
        $this->loader->add_action('woocommerce_order_status_refunded', $plugin_admin, 'order_refunded' );
        // user profile page in admin
        $this->loader->add_action('show_user_profile', $plugin_admin, 'user_profile_subscription_fields' );
        $this->loader->add_action('edit_user_profile', $plugin_admin, 'user_profile_subscription_fields' );
        $this->loader->add_action('personal_options_update', $plugin_admin, 'save_user_profile' );
        $this->loader->add_action('edit_user_profile_update', $plugin_admin, 'save_user_profile' );


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Al_Custom_Subscription_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		// hide category from products page
//        $this->loader->add_filter('wc_product_dropdown_categories_get_terms_args', $plugin_public, 'hide_category', 1);
        // subscription actions after order is completed
//        $this->loader->add_action( 'woocommerce_thankyou', $plugin_public, 'order_created', 100 );
        // disable buy button for subscription product if customer has not completed order with subscription products
        $this->loader->add_action( 'template_redirect', $plugin_public, 'disable_buy_button' );
        // metabox in wp-admin for selecting subscription life
        $this->loader->add_action('woocommerce_product_options_general_product_data', $plugin_public, 'disable_buy_button' );
        // cart can have only one subscription type product
        $this->loader->add_filter('woocommerce_add_to_cart_validation', $plugin_public, 'add_to_cart_validation', 10, 2 );
        // add Subscription tab in account page
        $this->loader->add_filter('woocommerce_account_menu_items', $plugin_public, 'account_page_tabs');
        $this->loader->add_action('init', $plugin_public, 'account_page_tab_endpoint');
        $this->loader->add_action('woocommerce_account_al_subscription_endpoint', $plugin_public, 'account_subscription_page');

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Al_Custom_Subscription_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
